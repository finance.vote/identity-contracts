import pytest
from numpy.random import uniform, bytes, randint
from brownie import web3, PaymentSplitter
from brownie.network.account import Account

def get_uniform_distribution(n, target_sum):
    samples = [uniform() for _ in range(n)]
    s = sum(samples)
    normalized = [int(target_sum * (x / s)) for x in samples]
    normalized[-1] = target_sum - sum(normalized[:-1])
    assert sum(normalized) == target_sum
    return normalized


def get_random_addresses(n):
    return [Account('0x' + bytes(20).hex()) for _ in range(n)]


def randomize_payments(splitter, accounts, recipients):
    num_payments = randint(5, 20)
    num_recipients = len(recipients)
    release_threshold = uniform()
    total_sent = 0
    precision = splitter.PRECISION()
    for j in range(num_payments):
        amount = int(uniform() * 1e16)
        total_sent += amount
        tx = accounts[0].transfer(to=splitter, amount=amount)
        assert tx.events['PaymentReceived']['from'] == accounts[0]
        assert tx.events['PaymentReceived']['amount'] == amount
        assert len(tx.events) == 1

        if uniform() < release_threshold:
            tx = splitter.release()
            assert len(tx.events['PaymentDisbursed']) == num_recipients

    tx = splitter.release()
    assert len(tx.events['PaymentDisbursed']) == num_recipients
    current_balances = [a.balance() for a in recipients]
    print('current_balances', current_balances)
    print('total_sent', total_sent)
    epsilon = num_recipients * num_payments
    assert abs(sum(current_balances) - total_sent) < epsilon
    for i, recipient in enumerate(recipients):
        balance = current_balances[i]
        ben = splitter.beneficiaries(i)
        assert ben == recipient
        shares = splitter.sharesPerCapita(recipient)
        sent_share = int(total_sent * shares / precision)
        assert abs(sent_share - balance) < epsilon


def test_payments_simple(PaymentSplitter, accounts):
    recipients = get_random_addresses(5)
    print('recipients', recipients)
    assert all([x.balance() == 0 for x in recipients])
    p = accounts[0].deploy(PaymentSplitter, recipients, [200000] * 5)
    accounts[0].transfer(to=p, amount=1e18)
    tx = p.release()
    assert p.balance() == 0
    assert sum([x.balance() for x in recipients]) == 1e18


def test_payments(PaymentSplitter, accounts):
    for i in range(randint(3, 10)):
        num_recipients = randint(1, 20)
        recipients = get_random_addresses(num_recipients)
        assert all([x.balance() == 0 for x in recipients])
        shares = [x for x in get_uniform_distribution(num_recipients, 1000000)]
        print('shares', shares)
        print('num_recipients', num_recipients)
        splitter = accounts[0].deploy(PaymentSplitter, recipients, shares)
        randomize_payments(splitter, accounts, recipients)


def test_payments_to_contracts(PaymentSplitter, accounts):
    num_recipients = randint(5, 20)
    splitters = [accounts[0].deploy(PaymentSplitter, [accounts[2]], [1000000]) for i in range(num_recipients)]
    shares = [x for x in get_uniform_distribution(num_recipients, 1000000)]
    main_splitter = accounts[0].deploy(PaymentSplitter, splitters, shares)
    randomize_payments(main_splitter, accounts, splitters)
