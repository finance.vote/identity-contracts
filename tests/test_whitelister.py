import brownie
from brownie import web3, Whitelister, MerkleIdentity, MerkleLib
from brownie.network.account import Account

def add_address(whitelister, address):
    whitelister.addAddress(address)

def test_address_adding(Whitelister, accounts):
    whitelister = accounts[0].deploy(Whitelister, accounts[0])
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == False
    add_address(whitelister, accounts[1])
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == True

def test_address_removing(Whitelister, accounts):
    whitelister = accounts[0].deploy(Whitelister, accounts[0])
    add_address(whitelister, accounts[1])
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == True
    whitelister.removeAddress(accounts[1])
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == False

def test_address_adding_from_non_management(Whitelister, accounts):
    whitelister = accounts[0].deploy(Whitelister, accounts[0])
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == False
    with brownie.reverts():
        whitelister.addAddress(accounts[1], {'from': accounts[2]})
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == False

def test_address_removing_from_non_management(Whitelister, accounts):
    whitelister = accounts[0].deploy(Whitelister, accounts[0])
    add_address(whitelister, accounts[1])
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == True
    with brownie.reverts():
        whitelister.removeAddress(accounts[1], {'from': accounts[2]})
    is_whitelisted = whitelister.whitelistedAddresses(accounts[1])
    assert is_whitelisted == True

def get_test_merkle_data():
    return ["0x61a620ca44e2f2f0e23dd478dee38ae693d9eb8232c48205d04fc239aebb1b6b", "0x0000000000000000000000000000000000000000000000000000000000000000", "0x506dfd5d886dCC5Cd10740833E166E517eA8042d", "0xf423151947Eb2FbF3c4a9924425cF8a898497744", "0x61127351206ecc3F5aAa2e000D125527dfDe01e1", 1, 1]

def test_allowing_whitelisted_address(Whitelister, MerkleIdentity, MerkleLib, accounts):
    merkle_lib = accounts[0].deploy(MerkleLib)
    merkle_identity = accounts[0].deploy(MerkleIdentity, accounts[0])
    whitelister = accounts[0].deploy(Whitelister, accounts[0])
    add_address(whitelister, accounts[0])
    merkle_identity.setTreeAdder(whitelister.address)
    whitelister.addMerkleTree(merkle_identity.address, *get_test_merkle_data())

def test_blocking_not_whitelisted_address(Whitelister, MerkleIdentity, MerkleLib, accounts):
    merkle_lib = accounts[0].deploy(MerkleLib)
    merkle_identity = accounts[0].deploy(MerkleIdentity, accounts[0])
    whitelister = accounts[0].deploy(Whitelister, accounts[0])
    merkle_identity.setTreeAdder(whitelister.address)
    with brownie.reverts():
        whitelister.addMerkleTree(merkle_identity.address, *get_test_merkle_data())