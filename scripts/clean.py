import pandas as pd

filepath = './whitelist.csv'

df = pd.read_csv(filepath)

df['OGS'] = df['OGS'].str.lower()
df['CONNOISSEUR'] = df['CONNOISSEUR'].str.lower()
df['CHINWAGGERS'] = df['CHINWAGGERS'].str.lower()
df['PARTYSTARTERS'] = df['PARTYSTARTERS'].str.lower()
df['SHERPA'] = df['SHERPA'].str.lower()

df['CITIZENS'] = df['CITIZENS'].str.lower()

d = pd.concat([
    df[df['OGS'].duplicated() == False]['OGS'],
    df[df['PARTYSTARTERS'].duplicated() == False]['PARTYSTARTERS'],
    df[df['CHINWAGGERS'].duplicated() == False]['CHINWAGGERS'],
    df[df['CONNOISSEUR'].duplicated() == False]['CONNOISSEUR'],
    df[df['SHERPA'].duplicated() == False]['SHERPA']
])

v = d.value_counts()
vf = pd.DataFrame(columns=['address', 'count'])
vf['address'] = v.index
vf['count'] = v.values

citizens = pd.read_csv('./citizens.csv')
citizens['address'] = citizens['address'].str.lower()

v2 = pd.concat([citizens, vf])
v2 = v2[v2['address'].duplicated() == False]
print(v2)


[
{
    'name': 'public-dogs',
    'eligibility_root': '0x5501f22bbaf175d1cb6fac714527cbe72a7404e2f90c463c87de84fbedd336da',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 1,
    'total_allowance': 8000,
    'eligibility_index': 6,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.05',
    'price_index': 6,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 17
},
{
    'name': 'public-all',
    'total_allowance': 8000,
    'eligibility_contract': 'AmaluEligibility',
    'eligibility_address': '0x58Ac13a1C9e162f6f471391A4b3c788563eC6520',
    'address_allowance': 1,
    'eligibility_index': 1,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.1',
    'price_index': 7,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 16
},
{
    'name': 'final-5.csv',
    'eligibility_root': '0x5dba086734d37f96a6f3c719653dd6abb3c76eace5e39671a50fad4260f6efe5',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 6,
    'total_allowance': 800,
    'eligibility_index': 5,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.02',
    'price_index': 5,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 15
},
{
    'name': 'final-4.csv',
    'eligibility_root': '0xa5a16b384fde5ceec1bc3f4d293d058b7dce1e71964bb6d8a2e896fc847a9e16',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 5,
    'total_allowance': 200,
    'eligibility_index': 4,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.02',
    'price_index': 5,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 14
},
{
    'name': 'final-3.csv',
    'eligibility_root': '0x9f113d49dc9b9796939dbd282e992457b6e819045bd40ae68bc6bfd17eca95a0',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 4,
    'total_allowance': 300,
    'eligibility_index': 3,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.02',
    'price_index': 5,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 13
},
{
    'name': 'final-2.csv',
    'eligibility_root': '0xff2122c2560032e9339e90dca06fcd82e6c92aa6709209a147f3fd4d2ac37824',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 3,
    'total_allowance': 150,
    'eligibility_index': 2,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.05',
    'price_index': 6,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 12
},
{
    'name': 'final-1.csv',
    'eligibility_root': '0xad0a9d0be41c7dd9e7b222899f73d3a03e8298e2aaaa32d01f25b5b9a55ea919',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 2,
    'total_allowance': 700,
    'eligibility_index': 1,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x3D56FDa58b80fbCaA79081D79cCBf8bDbe353146',
    'price': '0.05',
    'price_index': 6,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 11
}
]



[
{
    'name': 'public-dogs',
    'eligibility_root': '0x5501f22bbaf175d1cb6fac714527cbe72a7404e2f90c463c87de84fbedd336da',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 1,
    'total_allowance': 8000,
    'eligibility_index': 6,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.05',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 19
},
{
    'name': 'public-all',
    'total_allowance': 8000,
    'eligibility_contract': 'AmaluEligibility',
    'eligibility_address': '0x58Ac13a1C9e162f6f471391A4b3c788563eC6520',
    'address_allowance': 1,
    'eligibility_index': 1,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.1',
    'price_index': 2,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 18
},
{
    'name': 'final-5.csv',
    'eligibility_root': '0x5dba086734d37f96a6f3c719653dd6abb3c76eace5e39671a50fad4260f6efe5',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 6,
    'total_allowance': 800,
    'eligibility_index': 5,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.02',
    'price_index': 3,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 24
},
{
    'name': 'final-4.csv',
    'eligibility_root': '0xa5a16b384fde5ceec1bc3f4d293d058b7dce1e71964bb6d8a2e896fc847a9e16',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 5,
    'total_allowance': 200,
    'eligibility_index': 4,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.02',
    'price_index': 3,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 23
},
{
    'name': 'final-3.csv',
    'eligibility_root': '0x9f113d49dc9b9796939dbd282e992457b6e819045bd40ae68bc6bfd17eca95a0',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 4,
    'total_allowance': 300,
    'eligibility_index': 3,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.02',
    'price_index': 3,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 22
},
{
    'name': 'final-2.csv',
    'eligibility_root': '0xff2122c2560032e9339e90dca06fcd82e6c92aa6709209a147f3fd4d2ac37824',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 3,
    'total_allowance': 150,
    'eligibility_index': 2,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.05',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 21
},
{
    'name': 'final-1.csv',
    'eligibility_root': '0xad0a9d0be41c7dd9e7b222899f73d3a03e8298e2aaaa32d01f25b5b9a55ea919',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 2,
    'total_allowance': 700,
    'eligibility_index': 1,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.05',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 1,
    'identity_index': 20
}
]

{
    'name': 'public-dogs',
    'eligibility_root': '0x5501f22bbaf175d1cb6fac714527cbe72a7404e2f90c463c87de84fbedd336da',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 5,
    'total_allowance': 8000,
    'eligibility_index': 7,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.025',
    'price_index': 4,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 26
},
{
    'name': 'public-all',
    'total_allowance': 8000,
    'eligibility_contract': 'AmaluEligibility',
    'eligibility_address': '0x58Ac13a1C9e162f6f471391A4b3c788563eC6520',
    'address_allowance': 5,
    'eligibility_index': 2,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.05',
    'price_index': 5,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 25
},

{
    'name': 'public-dogs',
    'eligibility_root': '0x5501f22bbaf175d1cb6fac714527cbe72a7404e2f90c463c87de84fbedd336da',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 5,
    'total_allowance': 8000,
    'eligibility_index': 7,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.05',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 28
},
{
    'name': 'public-all',
    'total_allowance': 8000,
    'eligibility_contract': 'AmaluEligibility',
    'eligibility_address': '0x58Ac13a1C9e162f6f471391A4b3c788563eC6520',
    'address_allowance': 5,
    'eligibility_index': 2,
    'price_gate_contract': 'FixedSplitPooledPriceGate',
    'price_gate_address': '0x4718b4569BBca2Ca91738B6eE37c28f013AA4D29',
    'price': '0.1',
    'price_index': 2,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 2,
    'identity_index': 27
},
{
    'name': 'chris-free',
    'total_allowance': 20,
    'eligibility_contract': 'WhitelistEligibility',
    'eligibility_address': '0x29acac010A8E365eAa5459E3598B4002807cbe85',
    'address_allowance': 20,
    'eligibility_index': 1,
    'price_gate_contract': 'AmaluPriceGate',
    'price_gate_address': '0x3CAf0F8c9f7379981985e9C373C5281F0883ACBe',
    'price': '0',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 3,
    'identity_index': 29
},
{
    'name': 'high-minter-free',
    'total_allowance': 100,
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x3A87b5A373ac823773Dc7f125CcBb2Be9018E446',
    'address_allowance': 1,
    'eligibility_index': 8,
    'price_gate_contract': 'AmaluPriceGate',
    'price_gate_address': '0x3CAf0F8c9f7379981985e9C373C5281F0883ACBe',
    'price': '0',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 3,
    'identity_index': 30
},
{
    'name': 'mmc-free',
    'total_allowance': 20,
    'eligibility_contract': 'WhitelistEligibility',
    'eligibility_address': '0x29acac010A8E365eAa5459E3598B4002807cbe85',
    'address_allowance': 1,
    'eligibility_index': 2,
    'price_gate_contract': 'AmaluPriceGate',
    'price_gate_address': '0x3CAf0F8c9f7379981985e9C373C5281F0883ACBe',
    'price': '0',
    'price_index': 1,
    'metadata_root': '0x3c126d9ea472877992c2d146f56560230b1fdf42c161559f6602ad68b7d263b3',
    'stage': 3,
    'identity_index': 29
},
{
    'name': 'sovryn-blockchain-whispers',
    'chain': 'rsk',
    'total_allowance': 100,
    'nft_address': '0x1999b48df6Da3fa7810b3cE3fCE3b1da4E819888',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x47E215784E7eda3258BDef870E4727eC4328ED56',
    'eligibility_root': '0x9203b00a18ebc807d5a3e727b1d6df2d44384101e9e57450915664800b5fb35e',
    'address_allowance': 2,
    'eligibility_index': 2,
    'price_gate_contract': 'FixedPricePassThruGate',
    'price_gate_address': '0xd8f96E4b9C3F57316608Ed1Fd0625f5b0D779524',
    'price': '0.001 ether', # actually rbtc
    'price_index': 1,
    'beneficiary_address': '0x027Cc12c40c3Cb9C50ef5Ab420429d81feA03a7f',
    'metadata_root': '0xe34b817353c7665614651d6bf7bbe9e1d971c8b28f69c245c5d1809b28000f27',
    'identity_address': '0x83Cafaa139ddAfD37a4959569F4D3610A04C509a',
    'identity_index': 2,
},
{
    'name': 'sbt-free',
    'chain': 'matic',
    'total_allowance': 100,
    'nft_address': '0x541FD34035aE94BEE1B6985b0555BDc58449dA96',
    'eligibility_contract': 'AmaluEligibility',
    'eligibility_address': '0xbD88cCbcEBE86DF1f8C85C29F3E9AD5113b2236e',
    'eligibility_root': '',
    'address_allowance': 1,
    'eligibility_index': 1,
    'price_gate_contract': 'AmaluPriceGate',
    'price_gate_address': '0x0cd13D6731F9b3f02DC31250b537cC2284800402',
    'price': '0 ether', # actually matic
    'price_index': 1,
    'beneficiary_address': '0xe9cc66ae636327ef3257F68cCC436AAa452B8463',
    'metadata_root': '0x32d6819c9d33283da8f1f850b7d93b35c502a0f10bf4016f26267aceabbd1ffb',
    'identity_address': '0xFc90786321E65B1FC3a790D30f6C3Ef98C715390',
    'identity_index': 3,
},
{
    'name': 'sbt-linkers',
    'chain': 'matic',
    'total_allowance': 100,
    'nft_address': '0x541FD34035aE94BEE1B6985b0555BDc58449dA96',
    'eligibility_contract': 'MerkleEligibility',
    'eligibility_address': '0x1999b48df6Da3fa7810b3cE3fCE3b1da4E819888',
    'eligibility_root': '',
    'address_allowance': 1,
    'eligibility_index': 3,
    'price_gate_contract': 'FixedPricePassThruGate',
    'price_gate_address': '0x909B876c904b1252E9De9A570B4ee95E5686f172',
    'price': '10 ether', # actually matic
    'price_index': 2,
    'beneficiary_address': '0xe9cc66ae636327ef3257F68cCC436AAa452B8463',
    'metadata_root': '0x32d6819c9d33283da8f1f850b7d93b35c502a0f10bf4016f26267aceabbd1ffb',
    'identity_address': '0xFc90786321E65B1FC3a790D30f6C3Ef98C715390',
    'identity_index': 5,
},
{
    'name': 'sbt-speedbump',
    'chain': 'matic',
    'total_allowance': 1000,
    'nft_address': '0x541FD34035aE94BEE1B6985b0555BDc58449dA96',
    'eligibility_contract': 'AmaluEligibility',
    'eligibility_address': '0xbD88cCbcEBE86DF1f8C85C29F3E9AD5113b2236e',
    'eligibility_root': '',
    'address_allowance': 1000,
    'eligibility_index': 2,
    'price_gate_contract': 'SpeedBumpPriceGate',
    'price_gate_address': '0x8f624A8E59D9AC77795D9DC036B3ECA8327f6Ae5',
    'price': '20 ether', # actually matic
    'price_index': 1,
    'beneficiary_address': '0xe9cc66ae636327ef3257F68cCC436AAa452B8463',
    'metadata_root': '0x32d6819c9d33283da8f1f850b7d93b35c502a0f10bf4016f26267aceabbd1ffb',
    'identity_address': '0xFc90786321E65B1FC3a790D30f6C3Ef98C715390',
    'identity_index': 4,
},
