const { promises } = require("fs");
const path = require("path");

(async () => {
  const contracts = await promises.readdir(path.join("build", "contracts"));
  for (let contract of contracts) {
    const file = await promises.readFile(
      path.join("build", "contracts", contract),
      {
        encoding: "utf-8",
      }
    );
    const { contractName, abi, networks } = JSON.parse(file);
    await promises.writeFile(
      path.join("build", "contracts", contract),
      JSON.stringify({
        contractName,
        abi,
        networks,
      })
    );
  }
})();
