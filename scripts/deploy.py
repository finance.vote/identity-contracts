from brownie import MerkleLib, AmaluEligibility, \
    MerkleEligibility, SpeedBumpPriceGate, FixedPriceGate, \
    AmaluPriceGate, MerkleIdentity, Incinerator, VoterID, accounts, network
import json
# from brownie.network.gas.strategies import GasNowStrategy
# from brownie.network import gas_price

print('network', network.chain.id)

def move_json(contract_names):
    deployment_map = json.load(open('./build/deployments/map.json', 'r'))
    for contract_name in contract_names:
        with open('./build/contracts/{}.json'.format(contract_name)) as f:
            j = json.load(f)
            j['networks'] = {
                "4": {
                    "address": deployment_map["4"][contract_name][0]
                },
                "333": {
                    "address": deployment_map['dev'][contract_name][0]
                }
            }
            json.dump(j, open('./app/src/abi/{}.json'.format(contract_name), 'w'), indent=4)

uniswap_router = '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
mgmt = '0x288fE43139741F91a8Cbb6F4adD83811c794851b'
tokens = {
    '1': '0x45080a6531d671DDFf20DB42f93792a489685e32',
    '3': '0xF7eF90B602F1332d0c12cd8Da6cd25130c768929',
    '4': '0x1999b48df6Da3fa7810b3cE3fCE3b1da4E819888'
}

# uniswap_routers = {
#     '1': '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
#     '3': '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D',
#     '4': '0x7a250d5630B4cF539739dF2C5dAcb4c659F2488D'
# }

def main():
    if network.chain.id in [333, 1337]:
        acct = accounts[0]
        tx_obj = {'from': acct }
    else:
        acct = accounts.load('mgmt')
        priority_fee = '2.01 gwei'
        tx_obj = {'from': acct, 'priority_fee': priority_fee}

    # gas_strategy = GasNowStrategy("fast")
    # gas_price(gas_strategy)

    if network.chain.id in [1, 3, 4]:
        publish_source = True
    else:
        publish_source = False

    if len(MerkleLib) == 0:
        MerkleLib.deploy(tx_obj, publish_source=publish_source)

    incinerator = Incinerator.deploy(uniswap_router, mgmt, tx_obj, publish_source=publish_source)
    identity = MerkleIdentity.deploy(mgmt, tx_obj, publish_source=publish_source)
    amalu_eligibility = AmaluEligibility.deploy(mgmt, identity, tx_obj, publish_source=publish_source)
    merkle_eligibilty = MerkleEligibility.deploy(mgmt, identity, tx_obj, publish_source=publish_source)
    fixed_price_gate = FixedPriceGate.deploy(mgmt, tx_obj, publish_source=publish_source)
    speed_bump_price_gate = SpeedBumpPriceGate.deploy(mgmt, tx_obj, publish_source=publish_source)
    nft = VoterID.deploy(mgmt, identity, 'Shibaku', 'SHIBAKU', tx_obj, publish_source=publish_source)
    # move_json()

