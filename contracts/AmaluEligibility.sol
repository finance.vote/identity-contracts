// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IEligibility.sol";

contract AmaluEligibility is IEligibility {

    struct Gate {
        uint maxWithdrawalsAddress;
        uint maxWithdrawalsTotal;
        uint totalWithdrawals;
    }

    mapping (uint => Gate) public gates;
    mapping(uint => mapping(address => uint)) public timesWithdrawn;
    uint public numGates = 0;

    address public gateMaster;

    constructor (address _gateMaster) {
        gateMaster = _gateMaster;
    }

    function addGate(uint maxWithdrawalsAddress, uint maxWithdrawalsTotal) external returns (uint) {
        numGates += 1;

        gates[numGates] = Gate(maxWithdrawalsAddress, maxWithdrawalsTotal, 0);
        return numGates;
    }

    function getGate(uint index) external view returns (uint, uint, uint) {
        Gate memory gate = gates[index];
        return (gate.maxWithdrawalsAddress, gate.maxWithdrawalsTotal, gate.totalWithdrawals);
    }

    function isEligible(uint index, address recipient, bytes32[] memory) public override view returns (bool eligible) {
        Gate memory gate = gates[index];
        return timesWithdrawn[index][recipient] < gate.maxWithdrawalsAddress && gate.totalWithdrawals < gate.maxWithdrawalsTotal;
    }

    function passThruGate(uint index, address recipient, bytes32[] memory) external override {
        require(msg.sender == gateMaster, "Only gatemaster may call this.");

        // close re-entrance gate, prevent double withdrawals
        require(isEligible(index, recipient, new bytes32[](0)), "Address is not eligible");

        timesWithdrawn[index][recipient] += 1;
        Gate storage gate = gates[index];
        gate.totalWithdrawals += 1;
    }
}
