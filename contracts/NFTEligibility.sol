// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IEligibility.sol";
import "../interfaces/IERC721.sol";

contract NFTEligibility is IEligibility {

    address public management;
    address public gateMaster;

    struct Gate {
        address nft;
        uint maxWithdrawalsTotal;
        uint totalWithdrawals;
    }

    mapping (uint => Gate) public gates;
    uint public numGates = 0;

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address _mgmt, address _gateMaster) {
        management = _mgmt;
        gateMaster = _gateMaster;
    }

    function addGate(address nft, uint maxWithdrawalsTotal) external managementOnly returns (uint index) {
        // increment the number of roots
        numGates += 1;

        gates[numGates] = Gate(nft, maxWithdrawalsTotal, 0);
        return numGates;
    }

    function getGate(uint index) external view returns (address, uint, uint) {
        Gate memory gate = gates[index];
        return (gate.nft, gate.maxWithdrawalsTotal, gate.totalWithdrawals);
    }

    function isEligible(uint index, address recipient, bytes32[] memory) public override view returns (bool eligible) {
        Gate memory gate = gates[index];
        return IERC721(gate.nft).balanceOf(recipient) > 0 && gate.totalWithdrawals < gate.maxWithdrawalsTotal;
    }

    function passThruGate(uint index, address recipient, bytes32[] memory proof) external override {
        require(msg.sender == gateMaster, "Only gatemaster may call this.");

        // close re-entrance gate, prevent double withdrawals
        require(isEligible(index, recipient, proof), "Address is not eligible");

        Gate storage gate = gates[index];
        gate.totalWithdrawals += 1;
    }
}