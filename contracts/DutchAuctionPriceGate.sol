// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IPriceGate.sol";

/// @title A factory pattern for a price gate that starts high and drops down linearly to a minimum price
/// @author metapriest, adrian.wachel, marek.babiarz, radoslaw.gorecki
/// @dev Note passing thru the gate forwards all gas, so beneficiary can be a contract, possibly malicious
contract DutchAuctionPriceGate is IPriceGate {

    // this represents a single gate
    struct Gate {
        uint startTime; // time that this gate was created, used to compute current price
        uint maxPrice;  // how much does it cost to pass thru it
        uint priceDecayPerSecond; // how much does the price decay each second
        uint minPrice; // minimum price that will be reached if no one buys for any higher price
        address beneficiary;  // who gets the eth that is paid
    }

    // count the gates
    uint public numGates;
    // array-like map of gate structs
    mapping (uint => Gate) public gates;

    error NotEnoughETH(address from, uint price, uint paid);
    error TransferETHFailed(address from, address to, uint amount);

    /// @notice This adds a price gate to the list of available price gates
    /// @dev Anyone can call this, adding gates that don't get connected to merkleIndex isn't useful
    /// @dev No sanity checks here on prices and decay, so if you make a broken gate, it's your fault
    /// @param _startTime start time of auction
    /// @param _maxPrice amount of wei required to pass thru the gate at the beginning
    /// @param _priceDecayPerSecond amount of wei that the price decreases by every second
    /// @param _minPrice amount of wei required to pass thru the gate after decay has finished
    /// @param _beneficiary who receives the ether
    function addGate(uint _startTime, uint _maxPrice, uint _priceDecayPerSecond, uint _minPrice, address _beneficiary) external {
        // prefix operator increments then evaluates, first gate is at index 1
        Gate storage gate = gates[++numGates];
        gate.startTime = _startTime;
        gate.maxPrice = _maxPrice;
        gate.priceDecayPerSecond = _priceDecayPerSecond;
        gate.minPrice = _minPrice;
        gate.beneficiary = _beneficiary;
    }

    /// @notice Get the cost of passing thru this gate
    /// @param index which gate are we talking about?
    /// @return _ethCost the amount of ether required to pass thru this gate
    function getCost(uint index) override public view returns (uint) {
        Gate storage gate = gates[index];
        // note that this throws if startTime > block.timestamp, this is intended
        uint timeDiff = block.timestamp - gate.startTime;
        uint decay = timeDiff * gate.priceDecayPerSecond;
        if (decay > gate.maxPrice - gate.minPrice) {
            return gate.minPrice;
        }
        // if this is < 0, then the conditional above will be triggered
        return gate.maxPrice - decay;
    }

    /// @notice Pass thru this gate, should be called by MerkleIndex
    /// @dev This can be called by anyone, devs can call it to test it on mainnet
    /// @param index which gate are we passing thru?
    function passThruGate(uint index, address sender) override external payable {
        Gate memory gate = gates[index];
        uint cost = getCost(index);
        if (msg.value < cost) {
            revert NotEnoughETH(sender, cost, msg.value);
        }

        // pass thru ether
        if (msg.value > 0) {
            // use .call so we can send to contracts, for example gnosis safe, re-entrance is not a threat here
            (bool sent,) = gate.beneficiary.call{value: cost}("");
            if (sent == false) {
                revert TransferETHFailed(address(this), gate.beneficiary, cost);
            }

            uint leftover = msg.value - cost;
            if (leftover > 0) {
                (bool sent2,) = sender.call{value: leftover}("");
                if (sent2 == false) {
                    revert TransferETHFailed(address(this), sender, leftover);
                }
            }
        }
    }
}
