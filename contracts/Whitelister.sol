// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IMerkleIdentity.sol";

contract Whitelister {
    mapping(address => bool) public whitelistedAddresses;

    address public management;

    error ManagementOnly(address notManagement);
    error WhitelistedOnly(address notWhitelisted);

    constructor(address mgmt) {
        management = mgmt;
    }

    function addAddress(address addr) public managementOnly {
        whitelistedAddresses[addr] = true;
    }

    function removeAddress(address addr) public managementOnly {
        whitelistedAddresses[addr] = false;
    }

    modifier managementOnly() {
        if (msg.sender != management) {
            revert ManagementOnly(msg.sender);
        }
        _;
    }

    modifier whitelistedOnly() {
        if (whitelistedAddresses[msg.sender] != true) {
            revert WhitelistedOnly(msg.sender);
        }
        _;
    }

    function addMerkleTree(
        address merkleIdentityAddress,
        bytes32 metadataMerkleRoot,
        bytes32 ipfsHash,
        address nftAddress,
        address priceGateAddress,
        address eligibilityAddress,
        uint256 eligibilityIndex,
        uint256 priceIndex
    ) whitelistedOnly() external returns (uint256) {
        return
            IMerkleIdentity(merkleIdentityAddress).addMerkleTree(
                metadataMerkleRoot,
                ipfsHash,
                nftAddress,
                priceGateAddress,
                eligibilityAddress,
                eligibilityIndex,
                priceIndex
            );
    }
}
