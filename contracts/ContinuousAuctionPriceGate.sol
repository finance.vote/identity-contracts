// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IPriceGate.sol";

contract ContinuousAuctionPriceGate is IPriceGate {

    struct Gate {
        uint priceIncreaseFactor;
        uint priceIncreaseDenominator;
        uint lastPrice;
        uint decayFactor;
        uint priceFloor;
        uint lastPurchaseBlock;
        address beneficiary;
    }

    mapping (uint => Gate) public gates;
    uint public numGates;
    address public management;

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor (address _management) {
        management = _management;
    }

    // change the management key
    function setManagement(address newMgmt) external managementOnly {
        management = newMgmt;
    }

    function addGate(uint priceFloor, uint priceDecay, uint priceIncrease, uint priceIncreaseDenominator, address beneficiary) external managementOnly {
        numGates += 1;
        Gate storage gate = gates[numGates];
        gate.priceFloor = priceFloor;
        gate.decayFactor = priceDecay;
        gate.priceIncreaseFactor = priceIncrease;
        gate.beneficiary = beneficiary;
    }

    function setPriceParameters(uint index, uint newPriceFloor, uint newPriceDecay, uint newPriceIncreaseFactor, uint newPriceIncreaseDenominator) external managementOnly {
        Gate storage gate = gates[index];
        gate.priceFloor = newPriceFloor;
        gate.decayFactor = newPriceDecay;
        gate.priceIncreaseFactor = newPriceIncreaseFactor;
        gate.priceIncreaseDenominator = newPriceIncreaseDenominator;
    }

    function getCost(uint index) override public view returns (uint _ethCost) {
        Gate memory gate = gates[index];
        uint decay = gate.decayFactor * (block.number - gate.lastPurchaseBlock);
        if (gate.lastPrice < decay + gate.priceFloor) {
            return gate.priceFloor;
        } else {
            return gate.lastPrice - decay;
        }
    }

    function passThruGate(uint index, address) override external payable {
        uint price = getCost(index);
        require(msg.value >= price, 'Please send more ETH');

        // bump up the price
        Gate storage gate = gates[index];
        gate.lastPrice = (price * gate.priceIncreaseFactor) / gate.priceIncreaseDenominator;
        gate.lastPurchaseBlock = block.number;

        if (msg.value > 0) {
            (bool sent, bytes memory data) = gate.beneficiary.call{value: msg.value}("");
            require(sent, 'ETH transfer failed');
        }
    }
}
