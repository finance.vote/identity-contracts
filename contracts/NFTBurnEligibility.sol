// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IEligibility.sol";
import "../interfaces/IVoterID.sol";

contract NFTBurnEligibility is IEligibility {

    address public management;
    address public gateMaster;

    struct Gate {
        address nft;
        uint maxWithdrawalsTotal;
        uint numWithdrawalsPerBurn;
        uint totalWithdrawals;
        mapping (address => uint) withdrawalCount;
    }

    mapping (uint => Gate) public gates;
    uint public numGates = 0;


    constructor(address _gateMaster) {
        gateMaster = _gateMaster;
    }

    function addGate(address nft, uint maxWithdrawalsTotal, uint numWithdrawalsPerBurn) external returns (uint index) {
        // increment the number of roots
        numGates += 1;

        Gate storage gate = gates[numGates];
        gate.nft = nft;
        gate.maxWithdrawalsTotal = maxWithdrawalsTotal;
        gate.numWithdrawalsPerBurn = numWithdrawalsPerBurn;
        // gate.totalWithdrawals = 0;
        return numGates;
    }

    function getGate(uint index) external view returns (address, uint, uint, uint) {
        Gate storage gate = gates[index];
        return (gate.nft, gate.maxWithdrawalsTotal, gate.numWithdrawalsPerBurn, gate.totalWithdrawals);
    }

    function isEligible(uint index, address recipient, bytes32[] memory) public override view returns (bool eligible) {
        Gate storage gate = gates[index];
        uint numBurned = IVoterID(gate.nft).getBurned(recipient).length;
        uint maxWithdrawals = numBurned * gate.numWithdrawalsPerBurn;
        bool notTooManyPer = gate.withdrawalCount[recipient] < maxWithdrawals;
        bool notTooManyTotal = gate.totalWithdrawals < gate.maxWithdrawalsTotal;
        return notTooManyPer && notTooManyTotal;
    }

    function passThruGate(uint index, address recipient, bytes32[] memory proof) external override {
        require(msg.sender == gateMaster, "Only gatemaster may call this.");

        // close re-entrance gate, prevent double withdrawals
        require(isEligible(index, recipient, proof), "Address is not eligible");

        Gate storage gate = gates[index];
        gate.totalWithdrawals++;
        gate.withdrawalCount[recipient]++;
    }
}