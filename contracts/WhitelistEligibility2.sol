// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IEligibility.sol";

contract WhitelistEligibility2 is IEligibility {

    struct Gate {
        mapping(address => bool) whitelist;
        mapping(address => uint) numWithdrawals;
        uint maxWithdrawals;
    }

    mapping (uint => Gate) public gates;
    uint public numGates = 0;

    address public gateMaster;
    address public management;
    bool public paused;

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor (address _mgmt, address _gateMaster) {
        gateMaster = _gateMaster;
        management = _mgmt;
    }

    // change the management key
    function setManagement(address newMgmt) external managementOnly {
        management = newMgmt;
    }

    function setPaused(bool _paused) external managementOnly {
        paused = _paused;
    }

    function setWhitelist(uint index, address target, bool value) external managementOnly {
        Gate storage gate = gates[index];
        gate.whitelist[target] = value;
    }

    function addGate(uint maxWithdrawals) external managementOnly returns (uint) {
        numGates += 1;
        Gate storage gate = gates[numGates];
        gate.maxWithdrawals = maxWithdrawals;
        return numGates;
    }

    function getGate(uint index) external view returns (uint) {
        Gate storage gate = gates[index];
        return (gate.maxWithdrawals);
    }

    function isEligible(uint index, address recipient, bytes32[] memory) public override view returns (bool eligible) {
        Gate storage gate = gates[index];
        return !paused && gate.whitelist[recipient] && gate.numWithdrawals[recipient] < gate.maxWithdrawals;
    }

    function passThruGate(uint index, address recipient, bytes32[] memory) external override {
        require(msg.sender == gateMaster, "Only gatemaster may call this.");
        // close re-entrance gate, prevent double withdrawals
        require(isEligible(index, recipient, new bytes32[](0)), "Address is not eligible");

        Gate storage gate = gates[index];
        gate.numWithdrawals[recipient] += 1;
    }
}
