// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IEligibility.sol";
import "./MerkleLib.sol";

contract VestingEligibility is IEligibility {
    using MerkleLib for bytes32;

    address public management;
    address public gateMaster;

    struct Tranche {
        uint totalNFTs;
        uint claimedNFTs;
        uint startTime;
        uint endTime;
        uint secondsPerNFT;
    }

    mapping (uint => bytes32) public merkleRoots;
    mapping (address => mapping (uint => bool)) public initialized;
    mapping (address => mapping (uint => Tranche)) public tranches;
    uint public numMerkleRoots = 0;

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor(address _mgmt, address _gateMaster) {
        management = _mgmt;
        gateMaster = _gateMaster;
    }

    function addGate(bytes32 _merkleRoot) external managementOnly returns (uint index) {
        // increment the number of roots
        numMerkleRoots += 1;

        merkleRoots[numMerkleRoots] = _merkleRoot;
        return numMerkleRoots;
    }

    function getGate(uint index, address recipient) external view returns (uint) {
        return getAvailableNFTs(index, recipient);
    }

    function getAvailableNFTs(uint merkleIndex, address recipient) public view returns (uint) {
        if (initialized[recipient][merkleIndex] == false) {
            return 0;
        }
        Tranche memory tranche = tranches[recipient][merkleIndex];
        if (block.timestamp >= tranche.endTime) {
            return tranche.totalNFTs - tranche.claimedNFTs;
        } else {
            uint nfts = block.timestamp - tranche.startTime / tranche.secondsPerNFT;
            return nfts - tranche.claimedNFTs;
        }
    }

    function isEligible(uint rootIndex, address recipient, bytes32[] memory) public override view returns (bool eligible) {
        return getAvailableNFTs(rootIndex, recipient) > 0;
    }

    function passThruGate(uint rootIndex, address recipient, bytes32[] memory proof) external override {
        require(msg.sender == gateMaster, "Only gatemaster may call this.");

        // close re-entrance gate, prevent double withdrawals
        require(isEligible(rootIndex, recipient, proof), "Address is not eligible");

        Tranche storage tranche = tranches[msg.sender][rootIndex];
        tranche.claimedNFTs += 1;
    }

    function initialize(uint merkleIndex, address destination, uint totalNFTs, uint startTime, uint endTime, bytes32[] memory proof) external {
        require(!initialized[destination][merkleIndex], "Already initialized");
        bytes32 leaf = keccak256(abi.encode(destination, totalNFTs, startTime, endTime));

        require(merkleRoots[merkleIndex].verifyProof(leaf, proof), "The proof could not be verified.");
        initialized[destination][merkleIndex] = true;
        uint secondsPerNFT = (endTime - startTime) / totalNFTs;
        tranches[destination][merkleIndex] = Tranche(
            totalNFTs,
            0,
            startTime,
            endTime,
            secondsPerNFT
        );
    }

}