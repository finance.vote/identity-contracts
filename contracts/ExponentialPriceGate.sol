// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

import "../interfaces/IPriceGate.sol";
import "../interfaces/IIncinerator.sol";

contract ExponentialPriceGate is IPriceGate {

    uint public lastEthCost;
    uint public numPurchases;
    uint public totalBurned;
    uint public exponent;
    address public management;
    address public burnToken;
    uint public constant PRECISION = 1e6;

    IIncinerator public incinerator;

    event ManagementUpdated(address oldManagement, address newManagement);
    event ExponentUpdated(uint oldExp, uint newExp);

    modifier managementOnly() {
        require (msg.sender == management, 'Only management may call this');
        _;
    }

    constructor (uint _initEthCost, address _management, address _incinerator, address _burnToken, uint _exponent) {
        require(_exponent >= PRECISION, 'Must set exponent higher than PRECISION');
        lastEthCost = _initEthCost;
        management = _management;
        incinerator = IIncinerator(_incinerator);
        burnToken = _burnToken;
        exponent = _exponent;
    }

    // change the management key
    function setManagement(address newMgmt) external managementOnly {
        address oldMgmt =  management;
        management = newMgmt;
        emit ManagementUpdated(oldMgmt, newMgmt);
    }

    function setExponent(uint newExponent) external managementOnly {
        require(newExponent >= PRECISION, 'Must set exponent higher than PRECISION');
        uint oldExp = exponent;
        exponent = newExponent;
        emit ExponentUpdated(oldExp, newExponent);
    }

    function getCost(uint) override public view returns (uint _initEthCost) {
        return (lastEthCost * exponent) / PRECISION;
    }

    function passThruGate(uint index, address) override external payable {
        uint cost = getCost(index);
        require(msg.value >= cost, 'Please send more ETH');
        lastEthCost = cost;

        // burn token cost
        if (msg.value > 0) {
            // use .call so we can send to contracts, for example gnosis safe, re-entrance is not a threat here
            (bool sent, bytes memory data) = address(incinerator).call{value: msg.value}("");            require(sent, 'ETH transfer failed');
        }
    }
}
