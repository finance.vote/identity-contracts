// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

/**
 * @dev Required interface of an NFT contract to be used by MerkleIdentity
 */
interface IVoterID {
    /**
        @notice Minting function
    */
    function createIdentityFor(address newId, uint tokenId, string calldata uri) external;

    /**
        @notice Who has the authority to override metadata uri
    */
    function owner() external view returns (address);

    /**
        @notice How many of these things exist?
    */
    function totalSupply() external view returns (uint);

    /**
        @notice Which tokens were burned by which address?
    */
    function getBurned(address _owner) external view returns (uint[] memory);

}