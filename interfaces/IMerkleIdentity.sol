// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

interface IMerkleIdentity {
  function addMerkleTree(
        bytes32 metadataMerkleRoot,
        bytes32 ipfsHash,
        address nftAddress,
        address priceGateAddress,
        address eligibilityAddress,
        uint eligibilityIndex,
        uint priceIndex) external returns (uint);
}