// SPDX-License-Identifier: GPL-3.0-only

pragma solidity 0.8.12;

interface IIncinerator {

    function incinerate(address tokenAddr, uint amountOutMin) external payable;
}
